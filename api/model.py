from pydantic import BaseModel


class User(BaseModel):
    id: int
    name: str
    email: str


class Task(BaseModel):
    id: int
    name: str
    status: bool
    owner: User
