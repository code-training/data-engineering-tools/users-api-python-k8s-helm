from fastapi import FastAPI
from model import User
from constants import PORT
import uvicorn

users = [
    User(id="1", name="alex", email="alex@test.com"),
    User(id="1", name="alex", email="alex@test.com"),
]

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/users")
async def get_users():
    return [u.dict() for u in users]


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=PORT)
