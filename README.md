# users-api-python-k8s-helm



##

# API (Fast API)

### Run server in dev mode
uvicorn main:app --reload --host 0.0.0.0 --port 9001

### Run server prod
python main.py


# Docker
docker build -t todo-api:1 . 
docker run -d --rm -p 9001:9001 --name c-todo-api todo-api:1


# K8s
k8s apply -f pod.yaml 


# Helm


